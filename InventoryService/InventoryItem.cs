﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryService
{
    /// <summary>
    /// Describes an inventory item.
    /// </summary>
    /// <remarks>TODO - implement dynamic number of sizes.</remarks>
    public class InventoryItem
    {
        public int ProductId { get; set; }
        public int Size_1 { get; set; }
        public int Size_2 { get; set; }
        public int Size_3 { get; set; }
    }
}
