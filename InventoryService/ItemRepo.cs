﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryService
{
    /// <summary>
    /// A "repository" for inventory items
    /// </summary>
    /// <remarks>Static classes have a variety of problems, but for this exercise, allows for
    /// suitable testability and sharing of resources</remarks>
    public static class ItemRepo
    {
        private static List<InventoryItem> _items;

        static ItemRepo()
        {
            _items = new List<InventoryItem>()
            {
                new InventoryItem() {ProductId = 1,
                                    Size_1 = 3,
                                    Size_2 = 5,
                                    Size_3 = 8},
                new InventoryItem() {ProductId = 2,
                                    Size_1 = 11,
                                    Size_2 = 4,
                                    Size_3 = 29},
                new InventoryItem() {ProductId = 3,
                                    Size_1 = 45,
                                    Size_2 = 23,
                                    Size_3 = 88}

            };
        }
        public static ICollection<InventoryItem> GetAll()
        {
            object obj = new object();
            lock (obj)
            {
                return _items;
            }
        }

        public static InventoryItem GetById(int id)
        {
            object obj = new object();
            lock (obj)
            {
                //Allow item not found exception if item not found
                return _items.First(i => i.ProductId == id);
            }
        }

        public static void AddNewItem(InventoryItem newItem)
        {
            object obj = new object();
            lock (obj)
            {
                if (_items.Count(i => i.ProductId == newItem.ProductId) == 0)
                    _items.Add(newItem);
                //otherwise??
            }
        }

        public static void RemoveItem(InventoryItem item)
        {
            object obj = new object();
            lock (obj)
            {
                _items.Remove(item);
            }
        }
    }
}
